#Author: Victoria Strakhova
#Student number: 2159641

#I done subtasks 1,2
#I done subtasks 3,4

##
dataset=input('input name of flight information dataset: ')
airports=input('input name of airport information dataset: ')
tfield=input('is it arrival or departure? ')
dfield=input('which data are You interested: ')
filter=input('input a limitations: ')
features={'FL_DATE':0,'airline':1,'plane':2,'ORIGIN':3,'DEST':4,'departure':5,'departures_delay':6,'arrival':7,'arrival_delays':8,'CANCELLED':9,'DIVERTED':10,'ad_carrier':11,'ad_weather':12,
          'ad_nas':13,'ad_security':14,'ad_late_aircraft':15}
##
# in this part I get data according to user choise and also clean it from empty spaces and missing data in format "",,"" , if user doesn't input any filters
if len(filter)==0:
    with open(dataset, mode='r') as file:
        data_names=file.readlines()
    if dfield=='flights':
      tfield_information=[]
      [tfield_information.append(i.split(',')[features.get(tfield)]) for i in data_names if len(i.split(',')[features.get(tfield)]) != 2 and len(i.split(',')[features.get(tfield)+1])!=0]
      dfield_information=[]
      [dfield_information.append(i.split(',')[features.get(tfield)]) for i in data_names if len(i.split(',')[features.get(tfield)+1])!=0 and len(i.split(',')[features.get(tfield)]) != 2]
    else:
      tfield_information=[]
      [tfield_information.append(i.split(',')[features.get(tfield)]) for i in data_names if len(i.split(',')[features.get(tfield)]) != 2 and len(i.split(',')[features.get(dfield)])!=0]
      dfield_information=[]
      [dfield_information.append(i.split(',')[features.get(dfield)]) for i in data_names if len(i.split(',')[features.get(dfield)])!=0 and len(i.split(',')[features.get(tfield)]) != 2]
    dfield_information = dfield_information[1::]
    tfield_information=tfield_information[1::]

##
# here I created dictionary that contains only USA cities as keys and city-codes as values, not USA codes not included because our data only for domestic airlines. The filtration was made on basis of difference in format between USA and different countries records
with open(airports, mode='r') as file:
    data_airport=file.readlines()
    city_list = []
    code_list = []
    for i in data_airport[1::]:
        if len(i.replace('"','').replace(':',',').split(','))>=3:
           if len(i.replace('"','').replace(':',',').split(',')[2])==3:
               city_list.append(i.replace('"','').replace(':',',').split(',')[1])
               code_list.append(i.replace('"', '').replace(':', ',').split(',')[0])
    citycode_dict={}
    for m, n in enumerate(city_list):
        if n in citycode_dict:
            citycode_dict[n].append(code_list[m])
        else:
            citycode_dict.update({n:[]})
            citycode_dict[n].append(code_list[m])
##
# if user inputs filters, dictionary created to choose only relevant data in future, for airline and plane we keep data as it is, and for d_city, a_city change it to relevant codes
if len(filter)!=0:
    filter_1=filter.replace(';','=').split('=')
    limitations=['airline','plane','d_city','a_city']
    values_for_filtration=[[],[],[],[]]
    [values_for_filtration[limitations.index(n)].append(filter_1[m+1]) for m,n in enumerate(filter_1) if n in limitations]
    if len(values_for_filtration[2])!=0:
        values_for_filtration[2]=citycode_dict.get(values_for_filtration[2][0])
    if len(values_for_filtration[3])!=0:
        values_for_filtration[3]=citycode_dict.get(values_for_filtration[3][0])
    filtration_dictionary={}
    limitation_codes=[1,2,3,4]
    [filtration_dictionary.update({limitation_codes[m]:n}) for m,n in enumerate(values_for_filtration)]
    working_dictionary = {}
    [working_dictionary.update({key:value}) for key,value in filtration_dictionary.items() if len(filtration_dictionary.get(key))!=0]

##
#function for filtration according to user input
def filtration(dataset, working_dictionary):
    with open(dataset, mode='r') as file:
        data_names = file.readlines()
        tfield_information=[]
        dfield_information=[]
        if dfield != 'flights':
            for n in data_names:
                a = 0
                for key, value in working_dictionary.items():
                    if n.replace('"', '').split(',')[key] in value:
                        a = a + 1
                if len(n.split(',')[features.get(dfield)])!= 0 and len(n.split(',')[features.get(tfield)])!= 2 and a==len(working_dictionary):
                    tfield_information.append(n.split(',')[features.get(tfield)])
                    dfield_information.append(n.split(',')[features.get(dfield)])
        else:
            for n in data_names:
                a = 0
                for key, value in working_dictionary.items():
                    if n.replace('"', '').split(',')[key] in value:
                        a = a + 1
                if len(n.split(',')[features.get(tfield)+1])!= 0 and len(n.split(',')[features.get(tfield)])!= 2 and a==len(working_dictionary):
                    tfield_information.append(n.split(',')[features.get(tfield)])
                    dfield_information.append(n.split(',')[features.get(tfield)])
    return([tfield_information,dfield_information])

##
#here we get relevant data after filtration
if len(filter)!=0:
    mask=filtration(dataset, working_dictionary)
    if len(mask[0])==0:
        print('No data collected.')
        exit()
    tfield_information=mask[0]
    dfield_information=mask[1]
##
#last step of data manipulation before counting, differentiates between flights and other choises
tfield_information_new=[]
[tfield_information_new.append(int(i.replace('"',"").replace('2400','0'))) for i in tfield_information]
dfield_information_new=[]
if dfield=='flights':
  [dfield_information_new.append(1) for i in tfield_information]
else:
  [dfield_information_new.append(float(i)) for i in dfield_information]
  for m,n in enumerate(dfield_information_new):
      if n<0:
          dfield_information_new[m]=0
##
#counting of relevant data according time intervals (2 hours)
def counting(tfield_information_new,dfield_information_new):
    time_list = [159, 359, 559, 759, 959, 1159, 1359, 1559, 1759, 1959, 2159, 2359]
    time_intervals_count = {0:0, 1:0, 2:0, 3:0, 4:0, 5:0, 6:0, 7:0, 8:0,9:0,10:0,11:0}
    for m, n in enumerate(tfield_information_new):
        i=0
        while n>time_list[i]:
            i=i+1
        tfield_information_new[m]=i
    for m,n in enumerate(dfield_information_new):
        new_value=time_intervals_count.get(tfield_information_new[m])+float(dfield_information_new[m])
        time_intervals_count.update({tfield_information_new[m]:new_value})
    return(time_intervals_count)
##
output=counting(tfield_information_new,dfield_information_new)
##
#design of output by specific format
def output_design(n):
    time_intervals = {0:' 0:00- 1:59',1:' 2:00- 3:59',2:" 4:00- 5:59",3:' 6:00- 7:59',4:' 8:00- 9:59',
                      5:'10:00-11:59', 6:'12:00-13:59', 7:'14:00-15:59',8:'16:00-17:59',9:'18:00-19:59',
                      10:'20:00-21:59', 11:'22:00-23:59'}
    values=n.values()
    star_value=(max(values)/20)
    time_list=[]
    values_list=[]
    for key,value in time_intervals.items():
        time_list.append(value)
        values_list.append(int(n.get(key)))
    mysumm=str(sum(values_list))
    maxlength=len(str(max(values_list)))
    stars_list=[]
    #number of stars
    for i in values_list:
        a=('*')*(int(i/star_value))+(' ')*(20-int(i/star_value))
        stars_list.append(a)
    # numbers aligned from right
    for i in values_list:
        values_list[values_list.index(i)]=str(i)
    values_list_adjusted=[]
    for i in values_list:
        d=(' ')*(maxlength-len(i))+i
        values_list_adjusted.append(d)
    lines=[]
    [lines.append(f'{m}: |{stars_list[n]}| {values_list_adjusted[n]}') for n,m in enumerate(time_list)]
    #line of _
    b=len(max(lines))*('_')
    lines.append(b)
    #place for sum
    c=(' ')*(len(max(lines))-len(mysumm))+mysumm
    lines.append(c)
    return([print(i) for i in lines])

##
output_design(output)









